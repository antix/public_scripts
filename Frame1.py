#Boa:Frame:Frame1

import wx
import Dialog1
import lxml.html as html
import sys
import urllib2
from os import linesep


def create(parent):
    return Frame1(parent)

[wxID_FRAME1, wxID_FRAME1BUTTON1, wxID_FRAME1TEXTCTRL1, wxID_FRAME1TEXTCTRL2, 
 wxID_FRAME1TEXTCTRL3, 
] = [wx.NewId() for _init_ctrls in range(5)]

class Frame1(wx.Frame):
    message = ''

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_FRAME1, name='', parent=prnt,
              pos=wx.Point(500, 259), size=wx.Size(446, 177),
              style=wx.DEFAULT_FRAME_STYLE,
              title='Cotranslate.net subtitles extractor')
        self.SetClientSize(wx.Size(446, 177))

        self.textCtrl1 = wx.TextCtrl(id=wxID_FRAME1TEXTCTRL1, name='URL',
              parent=self, pos=wx.Point(32, 24), size=wx.Size(400, 24), style=0,
              value=u'http://cotranslate.net/translations/subtitles/12759')

        self.textCtrl2 = wx.TextCtrl(id=wxID_FRAME1TEXTCTRL2, name='Cookie',
              parent=self, pos=wx.Point(32, 60), size=wx.Size(400, 24), style=0,
              value=u'SESSdada7e3a3ec270de442eca31e2fabcff=dm433jc12b37jdeclhibslg...')

        self.button1 = wx.Button(id=wxID_FRAME1BUTTON1, label='Download',
              name='download', parent=self, pos=wx.Point(32, 130),
              size=wx.Size(400, 24), style=0)
        self.button1.Bind(wx.EVT_BUTTON, self.OnButton1Button,
              id=wxID_FRAME1BUTTON1)

        self.textCtrl3 = wx.TextCtrl(id=wxID_FRAME1TEXTCTRL3, name='textCtrl3',
              parent=self, pos=wx.Point(32, 96), size=wx.Size(400, 24), style=0,
              value=u'output.srt')

    def __init__(self, parent):
        self._init_ctrls(parent)

    def OnButton1Button(self, event):
        #event.Skip()
        url = self.textCtrl1.GetValue()
        cookie = self.textCtrl2.GetValue()
        filename = self.textCtrl3.GetValue()
        self.download(url, cookie, filename)
        dlg = Dialog1.Dialog1(self)
        try:
            dlg.ShowModal()
        finally:
            dlg.Destroy()

    def download(self, url, cookie, filename = 'output.srt'):
        user_agent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36'
        headers = { 'User-Agent' : user_agent, 'Cookie': cookie }
        req = urllib2.Request(url, headers=headers)
        response = urllib2.urlopen(req)
        
        page = html.parse(response)
        rows = page.xpath('//table[@class="cotranslate_form"]')[0].findall('tr')
        data=[]
        
        try:
            for row in rows:
                if url.find('/subtitles/') != -1:
                    sub_time = [row.find_class('cotranslate_prefix')[0].text] # this class contains joined sub_index and sub_time
                elif url.find('/texts/') != -1:
                    sub_time = list(row.find_class('cotranslate_source_part')[0].itertext())[:2] # first 2 elements contains sub_index and sub_time
                else:
                    print 'Something goes wrong at %s' % row.text_content()
                    sub_time = []
        
                if len(row.find_class('cotranslate_translation_part')) > 0:
                    sub_text = [x for x in row.find_class('cotranslate_translation_part')[0].itertext()] # this class contains text of translation. we should interate because of <br> tags inside
                    #raise IndexError('test')
                elif len(row.find_class('cotranslate_source_part')) > 0:
                    sub_text = [x for x in list(row.find_class('cotranslate_source_part')[0].itertext())[1:]] # also we don't need first element
                else:
                    sub_text = ''
                data.extend(sub_time)
                data.extend(sub_text)
                data.append('')
        except IndexError as e:
            self.message = str(e) + linesep + row.text_content()
            return
        
        sub_file = open(filename, 'w')
        
        for line in data:
            sub_file.write(line.encode('utf-8') + linesep)
        
        sub_file.close()
        self.message = u'File saved as %s in current directory.' % filename
