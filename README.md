### Some public scripts:
 - cotranslate_extractor.py - subtitles extractor from http://cotranslate.net.
 You need to install python 2.7. Linux distros and probably Mac OS should have it already.
 Windows - https://www.python.org/downloads/windows/ or http://www.activestate.com/activepython/downloads
 Also you need to install lxml module - `pip install lxml`. On windows - http://stackoverflow.com/questions/4750806/how-to-install-pip-on-windows
 For some translations you will need a working cookie - just copy it from dev tool in your browser. Usage:
 `./cotranslate_extractor.py http://cotranslate.net/translations/subtitles/4132`
 or
 `./cotranslate_extractor.py http://cotranslate.net/translations/subtitles/4132 --filename The.Flash.2014.S01E04.srt --cookie "SESSdada7e3a1ec27012345eca31e2fabcff=lvj07212345hqpsoj8kq2nr1234"`
 - ...
