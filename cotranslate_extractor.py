#!/usr/bin/env python
# -*- coding: utf-8 -*-
import lxml.html as html
import argparse
import sys
import urllib2
import codecs
from os import linesep

parser = argparse.ArgumentParser(description='Extract subtitles from http://cotranslate.net')
parser.add_argument("url", metavar='URL', type=str, help="URL for parse")
parser.add_argument("-f", "--filename", metavar='FILE', type=str, help="write subtitle to FILE")
parser.add_argument("-c", "--cookie", metavar='COOKIE', type=str, help="cookie for autorization")

args = parser.parse_args()

user_agent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36'
headers = { 'User-Agent' : user_agent }
if args.cookie:
    headers['Cookie'] = args.cookie
req = urllib2.Request(args.url, headers=headers)
response = urllib2.urlopen(req)

page = html.parse(response)
if not args.filename:
    title = page.xpath('//h1[@class="title"]')[0].text
    filename = filter(lambda c: c.isalnum() or c in (' ','_'), title).replace(' ','_') + ".srt"
else:
    filename = filter(lambda c: c.isalnum() or c in (' ','_','.'), args.filename)

rows = page.xpath('//table[@class="cotranslate_form"]')[0].findall('tr')
data=[]

try:
    for row in rows:
        if args.url.find('/subtitles/') != -1:
            sub_time = [row.find_class('cotranslate_prefix')[0].text] # this class contains joined sub_index and sub_time
        elif args.url.find('/texts/') != -1:
            sub_time = list(row.find_class('cotranslate_source_part')[0].itertext())[:2] # first 2 elements contains sub_index and sub_time
        else:
            print 'Something goes wrong at %s' % row.text_content()
            sub_time = []

        if len(row.find_class('cotranslate_translation_part')) > 0:
            sub_text = [x for x in row.find_class('cotranslate_translation_part')[0].itertext()] # this class contains text of translation. we should interate because of <br> tags inside
        elif len(row.find_class('cotranslate_source_part')) > 0:
            sub_text = [x for x in list(row.find_class('cotranslate_source_part')[0].itertext())[1:]] # also we don't need first element
        else:
            sub_text = ''
        data.extend(sub_time)
        data.extend(sub_text)
        data.append('')
except IndexError as e:
    print e
    print row.text_content()
    sys.exit(1)

#sub_file = open(filename, 'w')
sub_file = codecs.open(filename, mode='w', encoding='utf-8-sig')

for line in data:
    #sub_file.write(line.encode('utf-8') + linesep)
    sub_file.write(line + linesep)

sub_file.close()
