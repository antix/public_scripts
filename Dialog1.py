#Boa:Dialog:Dialog1

import wx


def create(parent):
    return Dialog1(parent)

[wxID_DIALOG1, wxID_DIALOG1BUTTON1, wxID_DIALOG1STATICTEXT1, 
] = [wx.NewId() for _init_ctrls in range(3)]

class Dialog1(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_DIALOG1, name='', parent=prnt,
              pos=wx.Point(449, 378), size=wx.Size(321, 158),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'Result')
        self.SetClientSize(wx.Size(321, 158))
        self.Bind(wx.EVT_INIT_DIALOG, self.OnDialog1InitDialog)

        self.button1 = wx.Button(id=wxID_DIALOG1BUTTON1, label=u'OK',
              name='button1', parent=self, pos=wx.Point(112, 104),
              size=wx.Size(96, 40), style=0)
        self.button1.SetToolTipString(u'button2')
        self.button1.Bind(wx.EVT_BUTTON, self.OnButton1Button,
              id=wxID_DIALOG1BUTTON1)

        self.staticText1 = wx.StaticText(id=wxID_DIALOG1STATICTEXT1,
              label=prnt.message,
              name='staticText1', parent=self, pos=wx.Point(40, 24),
              size=wx.Size(256, 38), style=0)

    def __init__(self, parent):
        self._init_ctrls(parent)
        #self.staticText1.label='aaa'
        #self.button1.label='bbb'


    def OnButton1Button(self, event):
        #event.Skip()
        #self.staticText1.label='aaa'
        #self.button1.label='bbb'
        self.Close()

    def OnDialog1InitDialog(self, event):
        event.Skip()


